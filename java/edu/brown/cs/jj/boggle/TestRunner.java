package edu.brown.cs.jj.boggle;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestRunner {

	@Test
	public void toStringTest() {
		Board b = new Board("test");
		assertEquals("enter after" + b.toString(), "enter after");
	}

}
