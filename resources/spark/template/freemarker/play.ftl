<#assign content>

<div id="hideRules" align="center">
<button type="button" onclick="hideRules()" align="center">Hide Boggle rules</button>
</div>

<script>
  function hideRules() {
    console.log("Rules hidden");
    $("#rules").hide();
  }
</script>

<div id="showRules" align="center">
<button type="button" onclick="showRules()" align="center">Show Boggle rules</button>
</div>

<script>
  function showRules() {
    console.log("Rules shown");
    $("#rules").show();
  }
</script>
  
<div id="rules" align="center">
<br><b>Welcome to Boggle! The goal is to score as many points as possible.<br/> 
You can make words by going in any of the 8 directions from a letter. <br/> 
8+ letters = 11 points <br/> 
7 letters = 5 points <br/> 
6 letters = 3 points <br/> 
5 letters = 2 points <br/> 
4 letters = 1 point <br/> 
3 letters = 1 point <b/><br/>
<br><br/>
</div>

<#include "board.ftl">

<div id="entry-area-stuff" align="center">
<br />

<div id="timer">
<button type="button" onclick="createCountDown(180000)">Start Timer</button>
<br><br/>
<script>
var endTime = 0;

function createCountDown(timeRemaining) {
	console.log("Starting countdown");
	endTime = Date.now() + timeRemaining;
	$("#timer").html("<p>3:00</p>");
	setInterval("doCountDown();", 100);
}

function doCountDown() {
	console.log("Doing countdown!");
	var timeRemaining = endTime - Date.now();
	if (timeRemaining < 0) {
	  timeRemaining = 0;
	}
	var timeRemainingSeconds = Math.ceil(timeRemaining / 1000);
	var minutesRemaining = Math.floor(timeRemainingSeconds / 60);
	var secondsRemaining = timeRemainingSeconds % 60;
	var pad = "00";
	var secondsRemaining2 = (pad+secondsRemaining).slice(-pad.length);
	$("#timer").html("<p>" + minutesRemaining + ":" + secondsRemaining2 + "</p>");
	if (timeRemaining === 0) {
	  window.alert("Time's up!");
	  $("#word-submit-form").submit();
	}  
}

/*function createCountDown(timeRemaining) {
	do {
    	var startTime = Date.now();
    	timerNow = timeRemaining - ( Date.now() - startTime );
    	document.write(timerNow);
    	if (timerNow === 0) {
    		window.alert("Times up!");
    	}
    } while (timerNow >= 0);
}*/
</script>
</div>

<form method="POST" action="/results" id="word-submit-form">
  <textarea id="id_guesses" name="guesses" placeholder="Enter words here"></textarea>
  <input type="submit">
  <input type="hidden" name="board" value="${board.toString(',')}">
  <input type="hidden" name="id_token" id="results_post_id_token" value="ANON">
</form>

</div>

</#assign>
<#include "main.ftl">