<#assign content>

<#include "board.ftl">
<b><p>
<center>
  Your score was ${score}. Your daily high score is ${dailyHighScore}. Your alltime high score is ${everHighScore}.
  
</p>

<#if 0 < good?size >
You scored with these:
<ul>
<#list good as word>
   ${word}
</#list>
</ul>
</#if>

<#if 0 < bad?size >
These words didn't score
<ul>
<#list bad as word>
   ${word}
</#list>
</ul>
</#if>


<#if 0 < missed?size > <br/>
<a href="/play">Play another board.</a> 
<br/>
You missed these, a total of ${badScore}. <b/>
You were able to find ${percentage} percent of points. Your daily high score percentage is ${dailyHighPercent}. <br/>
Your alltime best percentage found is ${everHighPercent}.


<ul>
<#list missed as word>
   ${word}
</#list>
</ul>
</#if>
<b/>
</center>


</#assign>
<#include "main.ftl">