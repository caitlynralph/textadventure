package database;

import java.sql.*;

/*
 * This class creates our db, which is saved in \workshop-07-boggle-sprint-1\
 * This class relies on the SQLiteJDBC external .jar which is stored in referenced libs
 * 
 * 
 * 
 * 
 */
public class dbCreateDB {
	public static void main(String args[]) {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:boggleData.db");
			System.out.println("Opened database successfully");

			stmt = c.createStatement();
			String sql = "CREATE TABLE USERS " + "(USER TEXT PRIMARY KEY     NOT NULL,"
					+ " NUMGAMES           INT    NOT NULL, " + " HSPOINTS            INT     NOT NULL, "
					+ " HSPERCENT        CHAR(50))";
			stmt.executeUpdate(sql);
			System.out.println("USER Table created successfully");
			sql = "CREATE TABLE GAMES " + "(ID 		INTEGER	PRIMARY KEY		AUTOINCREMENT, "
					+ "USER           TEXT    NOT NULL, " + " POINTS            INT     NOT NULL, "
					+ " PERCENT        INT, " + "TIME		INT)";
			stmt.executeUpdate(sql);
			System.out.println("GAMES Table created successfully");
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		
	}
}