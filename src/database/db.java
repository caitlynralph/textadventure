package database;

import java.sql.*;
import java.util.Random;

public class db {
	/**
	 * Passes a SQL statment to the database
	 * 
	 * @param statement
	 *            The SQLite statement to run
	 */
	public static void rawSQLite(String statement) {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:boggleData.db");
			c.setAutoCommit(false);
			// System.out.println("Opened database successfully");

			stmt = c.createStatement();
			String sql = statement;
			stmt.executeUpdate(sql);
			stmt.close();
			c.commit();
			c.close();
			System.out.println("\nSuccessfully executed SQL:");
			System.out.println(statement);

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	/**
	 * Prints the database to the console
	 */
	public static void showDB() {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:boggleData.db");
			c.setAutoCommit(false);
			System.out.println("\n\nboggleData.db");

			System.out.print("\n\nUSERS TABLE");
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM USERS;");
			System.out.println("\nUSER--NUMGAMES--HSPOINTS--HSPERCENT");
			while (rs.next()) {
				String user = rs.getString("user");
				int numgames = rs.getInt("numgames");
				int hspoints = rs.getInt("hspoints");
				int hspercent = rs.getInt("hspercent");
				System.out.println(user + "  " + numgames + "  " + hspoints + "  " + hspercent);
			}
			rs.close();

			System.out.print("\n\nGAMES TABLE");
			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT * FROM GAMES;");
			System.out.println("\nID--USER--POINTS--PERCENT--TIME");
			while (rs.next()) {
				int id = rs.getInt("id");
				String user = rs.getString("user");
				int points = rs.getInt("points");
				int percent = rs.getInt("percent");
				long time = rs.getLong("time");
				System.out.println(id + "  " + user + "  " + points + "  " + percent + "  " + time);
			}
			rs.close();

			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		// System.out.println("\nDatabase shown successfully");
	}

	/**
	 * Adds a player to the database
	 * 
	 * @param user
	 *            username of the player
	 */
	public static void newPlayer(String user) {
		String userIDString = user;
		String toSQL = "INSERT INTO USERS (USER,NUMGAMES,HSPOINTS,HSPERCENT) " + "VALUES ('" + userIDString
				+ "', 0, 0, 0);";
		rawSQLite(toSQL);
		System.out.println("Successfully added " + userIDString + " to the database.");
	}
	
	
	/**
	 * Checks if user already exists in database
	 * @param user username of the player
	 * @return true if user exists, false otherwise
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static boolean userExists(String user) {
		Connection c = null;
		Statement stmt = null;
		
		
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Database connector not found");
		}
		
		try {
			c = DriverManager.getConnection("jdbc:sqlite:boggleData.db");

			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = null;
			rs = stmt.executeQuery("SELECT USER FROM USERS WHERE USER = '" + user + "';");
			// System.out.println("Row: " + rs.getRow());
			// System.out.println("User: " + rs.getString(1));
			boolean hasResults = rs.next();
			rs.close();
			stmt.close();
			c.close();
			return hasResults;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		return true;
	}

	public static void clearDB() {
		rawSQLite("DELETE FROM USERS; DELETE FROM GAMES;");
	}

	/**
	 * Adds a game to the database. time is found automatically
	 * 
	 * @param user
	 *            username of the player
	 * @param points
	 *            points earned
	 * @param percent
	 *            percentage of points found
	 */
	public static void newGame(String user, int points, int percent) {
		long time = System.currentTimeMillis();
		String toSQL = "INSERT INTO GAMES (ID, USER,POINTS,PERCENT,TIME) " + "VALUES " + "(null, '" + user + "', '"
				+ points + "', '" + percent + "', '" + time + "');";
		toSQL += "UPDATE USERS SET NUMGAMES = NUMGAMES + 1 WHERE USER = '" + user + "';";
		toSQL += "UPDATE USERS SET HSPOINTS = (SELECT MAX(POINTS) FROM GAMES WHERE USER = '" + user + "')WHERE USER = '"
				+ user + "';";
		toSQL += "UPDATE USERS SET HSPERCENT = (SELECT MAX(PERCENT) FROM GAMES WHERE USER = '" + user
				+ "')WHERE USER = '" + user + "';";
		rawSQLite(toSQL);
	}

	/**
	 * 
	 * @return time in milliseconds one week ago
	 */
	public static long oneWeekAgo()
	{
		long currentTime = System.currentTimeMillis();
		return currentTime - 604800000;
	}
	
	/**
	 * 
	 * @return time in milliseconds 24 hours ago
	 */
	public static long oneDayAgo()
	{
		long currentTime = System.currentTimeMillis();
		return currentTime - 864000000;
	}
	
	/**
	 * 
	 * @param mode
	 *            2 for daily, 1 for weekly, 0 for all-time
	 * @param user
	 *            the username of the player
	 * @return highest points earned on a board
	 */
	public static int getHighPoints(int mode, String user) {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:boggleData.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = null;
			if (mode == 0) {
				rs = stmt.executeQuery("SELECT POINTS FROM GAMES WHERE USER = '" + user + "';");
			} 
			else if (mode == 1) {
				rs = stmt.executeQuery("SELECT POINTS FROM GAMES WHERE USER = '" + user + "' AND TIME > " + oneWeekAgo() + ";");
			}
			else if (mode == 2) {
				rs = stmt.executeQuery("SELECT POINTS FROM GAMES WHERE USER = '" + user + "' AND TIME > " + oneWeekAgo() + ";");
			}
			int points = 0;
			while (rs.next()) {
				int i = rs.getInt("points");
				if (i > points)
					points = i;
			}
			rs.close();

			stmt.close();
			c.close();
			return points;
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return 0;
		
	}
	
	/**
	 * 
	 * @param mode
	 *            2 for daily, 1 for weekly, 0 for all-time
	 * @param user
	 *            the username of the player
	 * @return highest percent earned on a board
	 */
	public static int getHighPercent(int mode, String user) {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:boggleData.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = null;
			if (mode == 0) {
				rs = stmt.executeQuery("SELECT PERCENT FROM GAMES WHERE USER = '" + user + "';");
			} 
			else if (mode == 1) {
				rs = stmt.executeQuery("SELECT PERCENT FROM GAMES WHERE USER = '" + user + "' AND TIME > " + oneWeekAgo() + ";");
			}
			else if (mode == 2) {
				rs = stmt.executeQuery("SELECT PERCENT FROM GAMES WHERE USER = '" + user + "' AND TIME > " + oneWeekAgo() + ";");
			}
			int points = 0;
			while (rs.next()) {
				int i = rs.getInt("percent");
				if (i > points)
					points = i;
			}
			rs.close();

			stmt.close();
			c.close();
			return points;
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return 0;
		
	}

	public static void main(String args[]) {
		//newPlayer("jackie"); -- throws an error if jackie already exits in the database
		//newGame("jackie", 50, 37);
		clearDB();
		showDB();
	}

}
