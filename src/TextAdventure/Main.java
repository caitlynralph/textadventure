package TextAdventure;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;

import database.db;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import freemarker.template.Configuration;
import spark.ExceptionHandler;
import spark.Spark;
import spark.ModelAndView;
import spark.QueryParamsMap;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;
import spark.template.freemarker.FreeMarkerEngine;

public abstract class Main {
	public static void main(String[] args) {
		OptionParser parser = new OptionParser();

		parser.accepts("generate");

		OptionSpec<String> solveSpec = parser.accepts("solve").withRequiredArg().ofType(String.class);

		OptionSet options = parser.parse(args);

		if (options.has("generate")) {
			System.out.print(new Board().toString());
		} else if (options.has(solveSpec)) {
			Board provided = new Board(options.valueOf(solveSpec));
			System.out.println(provided.play());
		} else {
			runSparkServer();
			// PROBLEM M1. runSparkServer() is NOT an infinite loop. Your
			// program will get to
			// this line. So why doesn't it exit?
		}
	}

	private static void runSparkServer() {
		// We need to serve some simple static files containing CSS and
		// JavaScript.
		// This tells Spark where to look for urls of the form "/static/*".
		Spark.externalStaticFileLocation("src/main/resources/static");

		// Development is easier if we show exceptions in the browser.
		Spark.exception(Exception.class, new ExceptionPrinter());

		// We render our responses with the FreeMaker template system.
		FreeMarkerEngine freeMarker = createEngine();
		
		Spark.setPort(1976);

		Spark.get("/play", new PlayHandler(), freeMarker);
		Spark.post("/results", new ResultsHandler(), freeMarker);
	}

	private static class PlayHandler implements TemplateViewRoute {
		@Override
		public ModelAndView handle(Request req, Response res) {
			Board board = new Board();
			Map<String, Object> variables = ImmutableMap.of("title", "It's BOGGLE time!", "board", board);
			return new ModelAndView(variables, "play.ftl");
		}
	}

	private static class ResultsHandler implements TemplateViewRoute {
		@Override
		public ModelAndView handle(Request req, Response res) {
			QueryParamsMap qm = req.queryMap();

			Board board = new Board(qm.value("board"));
			Set<String> legal = board.play();

			Iterable<String> guesses = BREAKWORDS.split(qm.value("guesses").toLowerCase());

			// We prefer to specify the more general interface types when
			// declaring variables, even though we must instantiate a more
			// specific, concrete type (TreeSet).

			// PROBLEM M2. We also prefer to elide the types with "diamond
			// operator". Why?
			SortedSet<String> good = new TreeSet<>();
			SortedSet<String> bad = new TreeSet<>();
			int score = 0;
			int badScore = 0;
			double percentage = 1.0;
			for (String word : guesses) {
				if (legal.contains(word)) {
					score += Board.score(word);
					good.add(word);
				} else {
					bad.add(word);
				}
			}
			for (String word : legal) {
				badScore += Board.score(word);
			}
			percentage = (100.0 * score / badScore);

			// We know that 'missed' will also give results in sorted order,
			// based on:
			// http://guava-libraries.googlecode.com/svn/tags/release04/javadoc/com/google/common/collect/Sets.html#difference(java.util.Set,
			// java.util.Set)

			// PROBLEM M3. What does that documentation mean by a "view"?
			// Why don't we declare 'missed' as a SortedSet?
			Set<String> missed = Sets.difference(legal, good);

			// PROBLEM M4. We can't use ImmutableMap.of(), as in PlayHandler.
			// Why not?
			
			
			String idToken = qm.value("id_token");
			if (!db.userExists(idToken)) {
				db.newPlayer(idToken);
			}
			db.newGame(idToken, score, (int) Math.round(percentage));
			int dailyHighPercent = db.getHighPercent(2, idToken);
			int dailyHighScore = db.getHighPoints(2, idToken);
			int everHighPercent = db.getHighPercent(0, idToken);
			int everHighScore = db.getHighPoints(0, idToken);

			// This is the "Builder" pattern which is a nice way to create
			// complicated immutable objects.
			Map<String, Object> variables = new ImmutableMap.Builder<String, Object>().put("title", "Boggle: Results")
					.put("board", board).put("score", score).put("good", good).put("bad", bad).put("missed", missed)
					.put("percentage", percentage).put("badScore", badScore).put("dailyHighPercent", dailyHighPercent)
					.put("dailyHighScore", dailyHighScore).put("everHighPercent", everHighPercent)
					.put("everHighScore", everHighScore).build();
			return new ModelAndView(variables, "results.ftl");
		}

		private static final Splitter BREAKWORDS = Splitter.on(Pattern.compile("\\W+")).omitEmptyStrings();
	}

	// You need not worry about understanding what's below here.

	private static FreeMarkerEngine createEngine() {
		Configuration config = new Configuration();
		File templates = new File("src/main/resources/spark/template/freemarker");
		try {
			config.setDirectoryForTemplateLoading(templates);
		} catch (IOException ioe) {
			System.out.printf("ERROR: Unable use %s for template loading.\n", templates);
			System.exit(1);
		}
		return new FreeMarkerEngine(config);
	}

	private static final int INTERNAL_SERVER_ERROR = 500;

	private static class ExceptionPrinter implements ExceptionHandler {
		@Override
		public void handle(Exception e, Request req, Response res) {
			res.status(INTERNAL_SERVER_ERROR);
			StringWriter stacktrace = new StringWriter();
			try (PrintWriter pw = new PrintWriter(stacktrace)) {
				pw.println("<pre>");
				e.printStackTrace(pw);
				pw.println("</pre>");
			}
			res.body(stacktrace.toString());
		}
	}

}
