package TextAdventure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.google.common.base.CharMatcher;
import com.google.common.collect.ImmutableList;

/**
 * A single configuration of dice in a 4 x 4 layout. A Board contains four rows
 * and columns, numbered from 0,0 at the top left.
 */

/*
 * PROBLEM B1. Suppose we also wanted to keep track of the orientation of a
 * letter, so we could provide a more faithful UI that showed some letters
 * rotated. What changes would you make to represent this addition information?
 * What changes would you make to the interface Board provides?
 */

public class Board {
	// A Dictionary based on the Online Scrabble Players' Dictionary.
	private static final Dictionary OSPD = new Dictionary("src/main/resources/ospd4.txt");

	private static final int SIZE = 4;
	private final List<List<Character>> spots = new ArrayList<>();

	/*
	 * Create a new, random board by rolling the dice in a standard Boggle set.
	 */
	public Board() {
		List<Die> dice = new ArrayList<>(Die.ALL);
		Collections.shuffle(dice);
		for (int r = 0; r < SIZE; r++) {
			spots.add(new ArrayList<>());
			for (int c = 0; c < SIZE; c++) {
				spots.get(r).add(dice.get(SIZE * r + c).pick());
			}
		}
	}

	private static final CharMatcher LETTERS = CharMatcher.inRange('a', 'z');

	/**
	 * Create a specific board specified by the letters in s.
	 * 
	 * @param s
	 *            A string containing 16 letters in the range 'a'..'z' which are
	 *            placed in the board, row by row, starting from the left. Any
	 *            other letters are discarded.
	 */
	public Board(String s) {
		s = LETTERS.retainFrom(s.toLowerCase());
		for (int r = 0; r < SIZE; r++) {
			spots.add(new ArrayList<>());
			for (int c = 0; c < SIZE; c++) {
				spots.get(r).add(s.charAt(SIZE * r + c));
			}
		}
	}

	/**
	 * Return a human readable version of this Board.
	 * 
	 * @return 4 lines of 4 characters, representing the 4 rows of letters shown
	 *         on the dice in this Board.
	 */
	@Override
	public String toString() {
		return toString('\n');
	}

	/**
	 * Return a human readable version of this Board.
	 * 
	 * @param separator
	 *            A character used to separate the four rows.
	 * @return 4 string of 4 characters, joined by separator
	 */
	public String toString(char separator) {
		StringBuilder sb = new StringBuilder();
		for (List<Character> row : spots) {
			for (Character c : row) {
				sb.append(c);
			}
			sb.append(separator);
		}
		return sb.toString();
	}

	/**
	 * Return the String fragment represented by the Die face at the given row
	 * and column. It is a String because the letter 'q' yields 'qu'.
	 * 
	 * @param r
	 *            The row, running from 0 at the top, to 3 at the bottom.
	 * @param c
	 *            The colomn, running from 0 on the left to 3 on the right.
	 * @return The part of a word (usually one letter) that available at r, c.
	 */
	public String get(int r, int c) {
		if (r < 0 || c < 0) {
			return "";
		}
		if (r >= spots.size()) {
			return "";
		}
		List<Character> line = spots.get(r);
		if (c >= line.size()) {
			return "";
		}
		char letter = line.get(c);
		return letter == 'q' ? "qu" : ("" + letter);
	}

	private Collection<String> search(String sofar, int r, int c, Set<List<Integer>> used) {
		List<Integer> p = ImmutableList.of(r, c);
		if (used.contains(p)) {
			return Collections.<String> emptyList();
		}

		String s = get(r, c);
		if ("".equals(s)) {
			return Collections.<String> emptyList();
		}

		List<String> ans = new ArrayList<>();
		sofar += s;
		if (OSPD.contains(sofar) && sofar.length() > 2) {
			ans.add(sofar);
		}

		if (OSPD.containsPrefix(sofar)) {
			used.add(p);
			ans.addAll(search(sofar, r + 1, c + 1, used));
			ans.addAll(search(sofar, r + 1, c, used));
			ans.addAll(search(sofar, r + 1, c - 1, used));
			ans.addAll(search(sofar, r, c + 1, used));
			ans.addAll(search(sofar, r, c - 1, used));
			ans.addAll(search(sofar, r - 1, c + 1, used));
			ans.addAll(search(sofar, r - 1, c, used));
			ans.addAll(search(sofar, r - 1, c - 1, used));
			used.remove(p);
		}
		return ans;

	}

	public Set<String> play() {
		Set<String> all = new TreeSet<>();
		for (int r = 0; r < SIZE; r++) {
			for (int c = 0; c < SIZE; c++) {
				all.addAll(search("", r, c, new HashSet<List<Integer>>()));
			}
		}
		return all;
	}

	public static int score(String word) {
		int len = word.length();
		if (len >= 8) {
			return 11;
		} else if (len == 7) {
			return 5;
		} else if (len == 6) {
			return 3;
		} else if (len == 5) {
			return 2;
		} else if (len == 4 || len == 3) {
			return 1;
		}
		return 0;
	}

}
