/**
 * Provides the classes necessary create and solve boggle games, both
 * at the command-line and through a local webserver.
 */
package TextAdventure;