package TextAdventure;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Set;

/**
 * A set of words that can be queried for membership. A Dictionary can also
 * report whether a given string is the prefix (beginning part) of any word in
 * the Dictionary.
 *
 * @author John Jannotti
 */

public class Dictionary {
	/*
	 * 'words' and 'prefixes' are not only private but DO NOT have getters (like
	 * getWords()). This prevents callers from grabbing the words and
	 * potentially manipulating them. contains() and containsPrefix() let
	 * callers check membership, and that's good enough.
	 * 
	 * PROBLEM Di1. Find a way to provide safe access to all of the words in the
	 * Dictionary.
	 */

	private final Set<String> words;
	private final Set<String> prefixes;

	/**
	 * Build a dictionary from a file containing one word per line.
	 * 
	 * @param filename
	 *            The name of the file to read words from.
	 */
	public Dictionary(String filename) {
		try {
			words = new HashSet<>(Files.readAllLines(new File(filename).toPath(), StandardCharsets.UTF_8));
		} catch (IOException e) {
			throw new IllegalArgumentException(filename, e);
		}

		prefixes = new HashSet<>();
		for (String w : words) {
			for (int i = 1; i < w.length(); i++) {
				prefixes.add(w.substring(0, i));
			}
		}
	}

	/**
	 * Reports on the existence of a word in this Dictionary.
	 * 
	 * @param word
	 *            The string to test
	 * @return true if word is in this Dictionary, else false.
	 */
	public boolean contains(String word) {
		return words.contains(word);
	}

	/**
	 * Reports on whether a string is a prefix of any word in this Dictionary.
	 * 
	 * @param pref
	 *            The string to test
	 * @return true if pref is the beginning of some word in this Dictionary,
	 *         else false.
	 */
	public boolean containsPrefix(String pref) {
		return prefixes.contains(pref);
	}
}
