<!DOCTYPE html>
  <head>
    <meta charset="utf-8">
    <title>${title}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/html5bp.css">
    <link rel="stylesheet" href="css/main.css">
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="406158453968-5jg07g5toh4oqeh6r4vhtdse60d56dpi.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
  </head>
  <body onLoad="hideLogout();">
  
<div id="login-area" align="right">
<div id="login"><div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div></div>
<div id="logout"><a href="#" onclick="signOut();" id="logout-link">Sign out</a></div>
</div>

<script>
  function hideLogout() {
    console.log("Logout hidden");
    $("#logout").hide();
  }

  function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
    $("#login").show();
    $("#logout").hide();
  }
    
  function onSignIn(googleUser) {
    // Useful data for your client-side scripts:
    var profile = googleUser.getBasicProfile();
    //console.log("ID: " + profile.getId()); // Don't send this directly to your server!
    //console.log('Full Name: ' + profile.getName());
    //$("#login-area").html("Welcome "+profile.getName()+"!");
    $("#login").hide();
    $("#logout").show();
    //$("#logout-link").html("Welcome "+profile.getName()+"!");
    $("#logout").prepend("<p>Welcome "+profile.getName()+"!</p>");

    // The ID token you need to pass to your backend:
    var id_token = googleUser.getAuthResponse().id_token;
    $("#results_post_id_token").val(id_token);
  };
</script>
     
<h1 align="center">Let's Boggle!</h1>

${content}

<p align="center">Customize interface:</p>

<div id="font-picker" align="center">
<script>
function setFont(font) {
	document.body.style.fontFamily = font;
}
function setBgColor() {
	var color = $("#background-color-picker").val();
	document.body.style.backgroundColor = color;
}
function setBoardBgColor() {
	var color = $("#board-background-color-picker").val();
	$(".board").css("background-color", color);
}
</script>

<button type="button" onclick="setFont('times');">Set font to Times</button>
<button type="button" onclick="setFont('arial');">Set font to Arial</button>
<button type="button" onclick="setFont('courier');">Sent font to Courier</button>

</div>


<br/>
<div id="color-picker" align="center" width="100%">
Background Color: <input type="color" id="background-color-picker" onChange="setBgColor();" value="#FFFFFF"> <br />
Board Background Color: <input type="color" id="board-background-color-picker" onChange="setBoardBgColor();" value="#FFFFFF">
</div>


<script src="js/jquery-2.1.1.js"></script>
<script src="js/main.js"></script>
     
</body>
</html>
